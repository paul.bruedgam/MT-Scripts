#!/usr/bin/env python3
import sqlite3
import numpy as np
from optparse import OptionParser
import os.path

def main():
    parser = OptionParser("python3 measure_connections.py [Options]")
    parser.add_option("-c", "--ciphers", dest="ciphers", help="Which ciphers should be tested. Write like openssl ciphers output separated by :.",
            default="ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:"
            "OQSKEX-RLWE-BCNS15:OQSKEX-RLWE-BCNS15-ECDHE:"
            "OQSKEX-RLWE-NEWHOPE:OQSKEX-RLWE-NEWHOPE-ECDHE:"
            "OQSKEX-RLWE-MSRLN16:OQSKEX-RLWE-MSRLN16-ECDHE:"
            #"OQSKEX-LWE-FRODO-RECOMMENDED:OQSKEX-LWE-FRODO-RECOMMENDED-ECDHE:"
            "OQSKEX-SIDH-CLN16:OQSKEX-SIDH-CLN16-ECDHE")
    parser.add_option("-m", "--mode", dest="mode", help="choose rsa (default) or ecdsa mode", default="rsa")
    parser.add_option("-d", "--debug", dest="debug", help="activate debug output", default=False, action="store_true")
    parser.add_option("-f", "--file", dest="file", help="which file should be analyzed", default="measure_connections.db")

    (options, args) = parser.parse_args()

    mode = options.mode
    debug = options.debug
    file = options.file
    ciphers = options.ciphers
    analyzed_data = []
    dbconn = sqlite3.connect(file)
    cursor = dbconn.cursor()
    sliced_data = {}
    
    cursor.execute("SELECT * FROM measure")
    data = cursor.fetchall()
    for row in data:
        cipher = row[0]
        if cipher not in sliced_data:
            sliced_data[cipher] = []

        sliced_data[cipher].append({
            'iteration': row[1],
            'cons': row[2],
            'secs': row[3],
            'cons_sec': row[4],
            'bytes': row[5],
            'filesize': row[6],
            'sigmode': row[7]
        })

    for cipher in ciphers.split(':'):
        if "ECDHE-ECDSA-AES256-GCM-SHA384" in cipher and "rsa" in mode:
            pass
        elif "ECDHE-RSA-AES256-GCM-SHA384" in cipher and "ecdsa" in mode:
            pass
        else:
            if debug:
                print(cipher)
            cons_f1 = []
            secs_f1 = []
            cons_sec_f1 = []
            bytes_f1 = []
            cons_f2 = []
            secs_f2 = []
            cons_sec_f2 = []
            bytes_f2 = []
            cons_f3 = []
            secs_f3 = []
            cons_sec_f3 = []
            bytes_f3 = []
            cons_f4 = []
            secs_f4 = []
            cons_sec_f4 = []
            bytes_f4 = []
            for row in sliced_data[cipher]:
                pass
                if row['filesize'] == "1b":
                    if debug:
                        print(row)
                    cons_f1.append(row['cons'])
                    secs_f1.append(row['secs'])
                    cons_sec_f1.append(row['cons_sec'])
                    bytes_f1.append(row['bytes'])
                if row['filesize'] == "1kib":
                    cons_f2.append(row['cons'])
                    secs_f2.append(row['secs'])
                    cons_sec_f2.append(row['cons_sec'])
                    bytes_f2.append(row['bytes'])
                if row['filesize'] == "10kib":
                    cons_f3.append(row['cons'])
                    secs_f3.append(row['secs'])
                    cons_sec_f3.append(row['cons_sec'])
                    bytes_f3.append(row['bytes'])
                if row['filesize'] == "100kib":
                    cons_f4.append(row['cons'])
                    secs_f4.append(row['secs'])
                    cons_sec_f4.append(row['cons_sec'])
                    bytes_f4.append(row['bytes'])

            cons_f1_mean = np.mean(cons_f1)
            cons_f1_std = np.std(cons_f1)
            cons_sec_f1_mean = np.mean(cons_sec_f1)
            cons_sec_f1_std = np.std(cons_sec_f1)

            cons_f2_mean = np.mean(cons_f2)
            cons_f2_std = np.std(cons_f2)
            cons_sec_f2_mean = np.mean(cons_sec_f2)
            cons_sec_f2_std = np.std(cons_sec_f2)

            cons_f3_mean = np.mean(cons_f3)
            cons_f3_std = np.std(cons_f3)
            cons_sec_f3_mean = np.mean(cons_sec_f3)
            cons_sec_f3_std = np.std(cons_sec_f3)

            cons_f4_mean = np.mean(cons_f4)
            cons_f4_std = np.std(cons_f4)
            cons_sec_f4_mean = np.mean(cons_sec_f4)
            cons_sec_f4_std = np.std(cons_sec_f4)

            analyzed_data.append({
                'cipher': cipher,
                'sigmode': mode,
                'cons_sec_f1_mean': cons_sec_f1_mean,
                'cons_sec_f1_std': cons_sec_f1_std,
                'cons_sec_f2_mean': cons_sec_f2_mean,
                'cons_sec_f2_std': cons_sec_f2_std,
                'cons_sec_f3_mean': cons_sec_f3_mean,
                'cons_sec_f3_std': cons_sec_f3_std,
                'cons_sec_f4_mean': cons_sec_f4_mean,
                'cons_sec_f4_std': cons_sec_f4_std,
            })

    print()
    print('{0:30s} {1:12s} {2:12s} {3:12s} {4:10s} {5:10s} {6:10s} {7:10s} {8:10s} {9:10s}'.format(
                                                                            "Cipher", "1b(mean)", "1b(std)",
                                                                            "1kib(mean)", "1kib(std)", "10kib(mean)",
                                                                            "10kib(std)", "100kib(mean)",
                                                                            "100kib(std)", "Mode"))
    for i in range(0, len(analyzed_data)):
        print('{0:30s} {1:11f} {2:11f} {3:11f} {4:11f} {5:11f} {6:11f} {7:11f} {8:11f} {9:10s}'.format(
                                                                                analyzed_data[i]['cipher'],
                                                                                analyzed_data[i]['cons_sec_f1_mean'],
                                                                                analyzed_data[i]['cons_sec_f1_std'],
                                                                                analyzed_data[i]['cons_sec_f2_mean'],
                                                                                analyzed_data[i]['cons_sec_f2_std'],
                                                                                analyzed_data[i]['cons_sec_f3_mean'],
                                                                                analyzed_data[i]['cons_sec_f3_std'],
                                                                                analyzed_data[i]['cons_sec_f4_mean'],
                                                                                analyzed_data[i]['cons_sec_f4_std'],
                                                                                analyzed_data[i]['sigmode']))


if __name__ == "__main__":
    main()
