#!/usr/bin/env python3

import subprocess
import time
import json
import re

start_time = time.time()
openssl_path = "/opt/openssl/bin"
openssl_speed = openssl_path + "openssl speed "
ciphers = ["ecdhp256", "ecdsap256", "rsa3072", "oqskex_rlwe_bcns15", "oqskex_rlwe_newhope", "oqskex_rlwe_msrln16",
           "oqskex_lwe_frodo_recommended", "oqskex_sidh_cln16"]
iterations = 500
regex_number = '[sm]'
regex_word = '[)(]'
data = {}
file = "measure_standalone.json"


def values_to_array(cipher, iteration, alice0, bob0, alice1):
    if cipher not in data:
        data[cipher] = []

    data[cipher].append({
        'iteration': iteration,
        'alice0': alice0,
        'bob0': bob0,
        'alice1': alice1,
    })


def get_values(values, iteration):
    alice0 = 0.0
    alice1 = 0.0

    if "ecdh" in values:
        cipher = "ecdhp256"
        bob0 = float(re.sub(regex_number, '', values.split()[4]))*1000  # s to ms
        print(cipher, iteration, alice0, bob0, alice1)
        values_to_array(cipher, iteration, alice0, bob0, alice1)
    elif "ecdsa" in values:
        cipher = "ecdsap256"
        bob0 = float(re.sub(regex_number, '', values.split()[5]))*1000  # s to ms
        alice1 = float(re.sub(regex_number, '', values.split()[4]))*1000  # s to ms
        print(cipher, iteration, alice0, bob0, alice1)
        values_to_array(cipher, iteration, alice0, bob0, alice1)
    elif "rsa" in values:
        cipher = "rsa3072"
        bob0 = float(re.sub(regex_number, '', values.split()[4]))*1000  # s to ms
        alice1 = float(re.sub(regex_number, '', values.split()[3]))*1000  # s to ms
        print(cipher, iteration, alice0, bob0, alice1)
        values_to_array(cipher, iteration, alice0, bob0, alice1)
    elif "frodo" in values:
        cipher = "oqskex_"+re.sub(regex_word, '', values.split()[1])
        alice0 = float(re.sub(regex_number, '', values.split()[2]))
        bob0 = float(re.sub(regex_number, '', values.split()[4]))
        alice1 = float(re.sub(regex_number, '', values.split()[6]))
        print(cipher, iteration, alice0, bob0, alice1)
        values_to_array(cipher, iteration, alice0, bob0, alice1)
    elif "oqskex" in values:
        cipher = "oqskex_"+re.sub(regex_word, '', values.split()[1])
        alice0 = float(re.sub(regex_number, '', values.split()[3]))
        bob0 = float(re.sub(regex_number, '', values.split()[5]))
        alice1 = float(re.sub(regex_number, '', values.split()[7]))
        print(cipher, iteration, alice0, bob0, alice1)
        values_to_array(cipher, iteration, alice0, bob0, alice1)


def main():
    for i in range(1, iterations + 1):
        for cipher in ciphers:
            print(str(i)+". Iteration:\t", cipher)
            openssl_output = subprocess.getoutput(openssl_speed + cipher)
            if cipher == "ecdhp256":
                get_values(openssl_output.split('\n')[6], i)
            elif cipher == "ecdsap256" or cipher == "rsa3072":
                get_values(openssl_output.split('\n')[7], i)
            elif "oqskex" in cipher:
                get_values(openssl_output.split('\n')[8], i)
            else:
                exit()

    with open(file, 'w') as outfile:
        json.dump(data, outfile)

    end_time = time.time()
    runtime = end_time - start_time
    print(runtime)
    if int(runtime) < 60:
        print("\n--- %s Sekunden ---" % runtime)
    elif int(runtime) >= 60 and int(runtime) < 3600:
        print("\n--- %s Minuten ---" % (runtime/60))
    else:
        print("\n--- %s Stunden ---" % (runtime/3600))


if __name__ == "__main__":
    main()
