#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
SSLTARGET="/opt/openssl"
BUILDDIR="/root/build"
if [ "$1" == "overwrite" ]
  then
    SSLTARGET="/usr"
fi

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

MAKE=$(which make)
GCC=$(which gcc)
UNZIP=$(which unzip)

if [ -z "$MAKE"] || [ -z "$GCC" ] || [ -z "$UNZIP" ]
  then
    printf "\n${GREEN}Install Dependencies for Compiling${NC}\n"
    apt-get update && apt-get install make gcc unzip -y
fi

if [ -d "$BUILDDIR/openssl" ]; then
    printf "\n${GREEN}Remove old OpenSSL Builddir${NC}\n"
    rm -rf $BUILDDIR/openssl
fi

if [ ! -d "$BUILDDIR" ]; then
    printf "\n${GREEN}Create Builddir${NC}\n"
    mkdir -p $BUILDDIR
fi

cd /root/build

# Modified Fork of https://github.com/open-quantum-safe/openssl.git
git clone --branch OpenSSL_1_0_2-stable https://paul_bruedgam@bitbucket.org/paul_bruedgam/openssl.git
cd openssl

printf "\n${GREEN}Configure for Compiling${NC}\n"
./config --prefix=$SSLTARGET --openssldir=$SSLTARGET

printf "\n${GREEN}Make Dependencies${NC}\n"
make depend

printf "\n${GREEN}Start Compiling${NC}\n"
make

if [ -d "$SSLTARGET" ]; then
    printf "\n${GREEN}Remove old OpenSSL Installdir${NC}\n"
    rm -rf $SSLTARGET
fi

printf "\n${GREEN}Make Installation${NC}\n"
make install

printf "\n${GREEN}Verify Installation${NC}\n"
$SSLTARGET/bin/openssl version
