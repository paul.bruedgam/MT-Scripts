#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
OPENSSL_BINDIR="/opt/openssl/bin"
OPENSSL_CNFDIR="/opt/openssl"
CERT_DIR="/root/certs"
CIPHERS="ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:OQSKEX-RLWE-BCNS15:OQSKEX-RLWE-BCNS15-ECDHE:OQSKEX-RLWE-NEWHOPE:OQSKEX-RLWE-NEWHOPE-ECDHE:OQSKEX-RLWE-MSRLN16:OQSKEX-RLWE-MSRLN16-ECDHE:OQSKEX-LWE-FRODO-RECOMMENDED:OQSKEX-LWE-FRODO-RECOMMENDED-ECDHE:OQSKEX-SIDH-CLN16:OQSKEX-SIDH-CLN16-ECDHE"
echo > tls_server_rsa.log
echo > tls_server_ecdsa.log
if [ "$1" == "cert" ] || [ -e !key.pem ] || [ -e !cert.pem ]
	then
		if [ ! -d "$CERT_DIR" ]; then
		    printf "\n${GREEN}Create cert dir${NC}\n"
		    mkdir -p $CERT_DIR
		fi

		printf "\n${GREEN}Creating rsa certificate for the server.${NC}\n"
		$OPENSSL_BINDIR/openssl req \
			-x509 \
			-new \
			-newkey rsa:3072 \
			-keyout $CERT_DIR/rsa3072_key.pem \
			-nodes \
			-out $CERT_DIR/rsa3072_cert.pem \
			-sha256 \
			-days 365 \
			-subj "/C=DE/ST=Mecklenburg-Vorpommern/L=Stralsund/O=FH Stralsund/CN=pqc" \
			-config $OPENSSL_CNFDIR/openssl.cnf

		printf "\n${GREEN}Creating ecdsa certificate for the server.${NC}\n"
		$OPENSSL_BINDIR/openssl req \
			-x509 \
			-new \
    	-newkey ec:<($OPENSSL_BINDIR/openssl ecparam -name prime256v1) \
			-keyout $CERT_DIR/ecdsa_prime256v1_key.pem \
			-nodes \
			-out $CERT_DIR/ecdsa_prime256v1_cert.pem \
			-sha256 \
			-days 365 \
    	-subj "/C=DE/ST=Mecklenburg-Vorpommern/L=Stralsund/O=FH Stralsund/CN=pqc" \
			-config $OPENSSL_CNFDIR/openssl.cnf

		printf "\n${GREEN}Creating html files for TLS test.${NC}\n"
		dd if=/dev/zero of=1b.html  bs=1  count=1
		dd if=/dev/zero of=1kib.html  bs=1k  count=1
		dd if=/dev/zero of=10kib.html  bs=10k  count=1
		dd if=/dev/zero of=100kib.html  bs=100k  count=1
		exit
fi

if [ "$1" == "rsa" ]
	then
		printf "\n${GREEN}Starting tls-server with oqskex ciphers in rsa-mode.${NC}\n"
		$OPENSSL_BINDIR/openssl s_server -key $CERT_DIR/rsa3072_key.pem -cert $CERT_DIR/rsa3072_cert.pem -accept 4433 -HTTP -cipher $CIPHERS -named_curve prime256v1
elif [ "$1" == "ecdsa" ]
	then
		printf "\n${GREEN}Starting tls-server with oqskex ciphers in ecdsa-mode.${NC}\n"
    $OPENSSL_BINDIR/openssl s_server -key $CERT_DIR/ecdsa_prime256v1_key.pem -cert $CERT_DIR/ecdsa_prime256v1_cert.pem -accept 4433 -HTTP -cipher $CIPHERS -named_curve prime256v1
else
	echo "Something goes wrong"
	exit
fi
