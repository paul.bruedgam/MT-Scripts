#!/usr/bin/env python3
import json
import numpy as np
from optparse import OptionParser

def main():
    parser = OptionParser("python3 measure_connections.py [Options]")
    parser.add_option("-c", "--ciphers", dest="ciphers", help="Which ciphers should be tested. Write like openssl ciphers output separated by :.",
            default="ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:"
            "OQSKEX-RLWE-BCNS15:OQSKEX-RLWE-BCNS15-ECDHE:"
            "OQSKEX-RLWE-NEWHOPE:OQSKEX-RLWE-NEWHOPE-ECDHE:"
            "OQSKEX-RLWE-MSRLN16:OQSKEX-RLWE-MSRLN16-ECDHE:"
            #"OQSKEX-LWE-FRODO-RECOMMENDED:OQSKEX-LWE-FRODO-RECOMMENDED-ECDHE:"
            "OQSKEX-SIDH-CLN16:OQSKEX-SIDH-CLN16-ECDHE")
    parser.add_option("-m", "--mode", dest="mode", help="choose rsa (default) or ecdsa mode", default="rsa")
    parser.add_option("-d", "--debug", dest="debug", help="activate debug output", default=False, action="store_true")
    parser.add_option("-f", "--file", dest="file", help="which file should be analyzed", default="measure_contime.json")

    (options, args) = parser.parse_args()

    mode = options.mode
    debug = options.debug
    file = options.file
    ciphers = options.ciphers
    analyzed_data = []

    with open(file) as json_file:
        data = json.load(json_file)
    for cipher in ciphers.split(':'):
        if mode == "rsa" and cipher == "ECDHE-ECDSA-AES256-GCM-SHA384":
            pass
        elif mode == "ecdsa" and cipher == "ECDHE-RSA-AES256-GCM-SHA384":
            pass
        else:
            con_time = [row['con_time'] for row in data[cipher]]
            con_time_mean = np.mean(con_time)
            con_time_std = np.std(con_time)

            analyzed_data.append({
                'cipher': cipher,
                'con_time_mean': con_time_mean,
                'con_time_std': con_time_std,
                'sigmode': data[cipher][0]['sigmode']
            })

    print()
    print('{0:29s} {1:10s} {2:12s} {3:11s}'.format("Cipher", "ConTime(mean)", "ConTime(std)", "Mode"))
    for i in range(0, len(analyzed_data)):
        print('{0:30s} {1:11f} {2:12f} {3:5s}'.format(analyzed_data[i]['cipher'], analyzed_data[i]['con_time_mean'],
                                                                           analyzed_data[i]['con_time_std'],
                                                                           analyzed_data[i]['sigmode']))


if __name__ == "__main__":
    main()
