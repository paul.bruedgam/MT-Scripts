#!/usr/bin/env python3
import json
import numpy as np


def main():
    ciphers = ["ecdhp256", "rsa4096", "oqskex_rlwe_bcns15", "oqskex_rlwe_newhope", "oqskex_rlwe_msrln16",
               "oqskex_sidh_cln16"]
    file = "measure_standalone.json"
    analyzed_data = []
    with open(file) as json_file:
        data = json.load(json_file)
    for cipher in ciphers:
        alice0 = [row['alice0'] for row in data[cipher]]
        bob0 = [row['bob0'] for row in data[cipher]]
        alice1 = [row['alice1'] for row in data[cipher]]
        alice0_mean = np.mean(alice0)
        alice0_std = np.std(alice0)
        bob0_mean = np.mean(bob0)
        bob0_std = np.std(bob0)
        alice1_mean = np.mean(alice1)
        alice1_std = np.std(alice1)

        analyzed_data.append({
            'cipher': cipher,
            'alice0_mean': alice0_mean,
            'alice0_std': alice0_std,
            'bob0_mean': bob0_mean,
            'bob0_std': bob0_std,
            'alice1_mean': alice1_mean,
            'alice1_std': alice1_std,
        })

    print()
    print('{0:29s} {1:10s} {2:12s} {3:12s} {4:10s} {5:10s} {6:10s}'.format("Cipher", "Alice0(mean)", "Alice0(std)",
                                                                           "Bob0(mean)", "Bob0(std)", "Alice1(mean)",
                                                                           "Alice1(std)"))
    for i in range(0, len(analyzed_data)):
        print('{0:30s} {1:11f} {2:11f} {3:11f} {4:11f} {5:11f} {6:11f}'.format(analyzed_data[i]['cipher'],
                                                                               analyzed_data[i]['alice0_mean'],
                                                                               analyzed_data[i]['alice0_std'],
                                                                               analyzed_data[i]['bob0_mean'],
                                                                               analyzed_data[i]['bob0_std'],
                                                                               analyzed_data[i]['alice1_mean'],
                                                                               analyzed_data[i]['alice1_std']))


if __name__ == "__main__":
    main()
