#!/usr/bin/env python3

import subprocess
import time
import json
from optparse import OptionParser
import sqlite3

start_time = time.time()
data = {}
dbname = 'measure_contime.db'
jfile = "measure_contime.json"
dbconn = sqlite3.connect(dbname)
cursor = dbconn.cursor()


class Bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[32m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'


    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


def values_to_db(cipher, iteration, con_time, sigmode):
    cursor.execute("INSERT INTO measure VALUES (?, ?, ?, ?)", (cipher, iteration, con_time, sigmode))
    dbconn.commit()
    cursor.execute('SELECT * FROM measure WHERE cipher=? and iteration=? and con_time=? and sigmode=?',
                   (cipher, iteration, con_time, sigmode))
    print(cursor.fetchall())


def values_to_array(cipher, iteration, con_time, sigmode):
    if cipher not in data:
        data[cipher] = []

    data[cipher].append({
        'iteration': iteration,
        'con_time': con_time,
        'sigmode': sigmode,
    })


def get_values(cipher, values, iteration, sigmode):
    con_time = values*1000 # s to ms
    values_to_db(cipher, iteration, con_time, sigmode)
    values_to_array(cipher, iteration, con_time, sigmode)


def main():
    parser = OptionParser("python3 measure_connections.py [Options]")
    parser.add_option("-o", "--openssl", dest="openssl", help="path to OpenSSL binary dir",
    default="/opt/openssl/bin")
    parser.add_option("-c", "--ciphers", dest="ciphers", help="Which ciphers should be tested. Write like openssl ciphers output separated by :.",
    default="ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:"
            "OQSKEX-RLWE-BCNS15:OQSKEX-RLWE-BCNS15-ECDHE:"
            "OQSKEX-RLWE-NEWHOPE:OQSKEX-RLWE-NEWHOPE-ECDHE:"
            "OQSKEX-RLWE-MSRLN16:OQSKEX-RLWE-MSRLN16-ECDHE:"
            #"OQSKEX-LWE-FRODO-RECOMMENDED:OQSKEX-LWE-FRODO-RECOMMENDED-ECDHE:"
            "OQSKEX-SIDH-CLN16:OQSKEX-SIDH-CLN16-ECDHE")
    parser.add_option("-t", "--target", dest="target", help="DNS or IP of the server", default="localhost")
    parser.add_option("-p", "--port", dest="port", help="port of the server", default="4433")
    parser.add_option("-i", "--iterations", dest="iterations", help="number of iterations (default=1)", default=1)
    parser.add_option("-m", "--mode", dest="mode", help="choose rsa (default) or ecdsa mode", default="rsa")
    parser.add_option("-d", "--debug", dest="debug", help="activate debug output", default=False, action="store_true")

    (options, args) = parser.parse_args()

    openssl_path = options.openssl
    ciphers = options.ciphers.split(':')
    destination = options.target
    port = str(options.port)
    iterations = int(options.iterations)
    mode = options.mode
    debug = options.debug

    # Create Table
    cursor.execute('''CREATE TABLE measure (cipher, iteration, con_time, sigmode)''')
    dbconn.commit()

    openssl_scli = openssl_path + "/openssl s_client "
    for i in range(1, iterations + 1):
        for c in ciphers:
            if mode == "rsa" and c == "ECDHE-ECDSA-AES256-GCM-SHA384":
                pass
            elif mode == "ecdsa" and c == "ECDHE-RSA-AES256-GCM-SHA384":
                pass
            else:
                scli_params = "-connect " + destination + ":" + port + " -cipher " + c + " < /dev/null"
                if debug:
                    print("\n" + Bcolors.OKBLUE + "Debug: " + openssl_scli + scli_params + Bcolors.ENDC)
                print("\n" + Bcolors.OKGREEN + str(i) + ". Iteration for Cipher " + c + ":" + Bcolors.ENDC)
                time1 = time.time()
                openssl_output = subprocess.getoutput(openssl_scli + scli_params)
                time2 = time.time()-time1
                if debug:
                    print(time2)
                time.sleep(0.5)
                try:
                    get_values(c, time2, i, mode)
                except Exception as err:
                    print("\n" + Bcolors.FAIL + str(err) + Bcolors.ENDC)
                    print(Bcolors.FAIL + openssl_output + Bcolors.ENDC)
                    dbconn.commit()
                    dbconn.close()

                    with open(jfile, 'w') as outfile:
                        json.dump(data, outfile)

                    exit()
        print("\n" + Bcolors.OKBLUE + "Hours passed: " + str((time.time()-start_time)/3600) + Bcolors.ENDC)
    dbconn.commit()
    dbconn.close()

    with open(jfile, 'w') as outfile:
        json.dump(data, outfile)

    end_time = time.time()
    runtime = end_time - start_time
    if int(runtime) < 60:
        print("\nSekunden:" + str(runtime))
    elif 60 <= int(runtime) < 3600:
        print("\nMinuten:" + str(runtime / 60))
    else:
        print("\nStunden:" + str(runtime / 3600))


if __name__ == '__main__':
    main()
