#!/bin/bash
OPENSSL_BINDIR="/opt/openssl/bin"
CIPHERS="ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:OQSKEX-RLWE-BCNS15:OQSKEX-RLWE-BCNS15-ECDHE:OQSKEX-RLWE-NEWHOPE:OQSKEX-RLWE-NEWHOPE-ECDHE:OQSKEX-RLWE-MSRLN16:OQSKEX-RLWE-MSRLN16-ECDHE:OQSKEX-LWE-FRODO-RECOMMENDED:OQSKEX-LWE-FRODO-RECOMMENDED-ECDHE:OQSKEX-SIDH-CLN16:OQSKEX-SIDH-CLN16-ECDHE"
TARGET=$1
IFS=':' read -ra CIPHERS <<< "$CIPHERS"    #Convert string to array
echo > tls_client.log

for C in "${CIPHERS[@]}"; do
  echo "##########################################################################" >> tls_client.log
  echo "# $C" >> tls_client.log
  echo "##########################################################################" >> tls_client.log
  echo >> tls_client.log
  echo "Q" | $OPENSSL_BINDIR/openssl s_client -connect $TARGET -cipher $C | tee -a tls_client.log
done
